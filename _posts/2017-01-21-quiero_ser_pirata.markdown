---
title: "¡Quiero ser pirata!"
author: Partido Interdimensional Pirata
layout: post
cover: "assets/covers/single/quiero_ser_pirata.png"
slider: "assets/covers/slider/quiero_ser_pirata.png"
signature: 0
toc: true
breaks: false
---

![](assets/images/quiero_ser_pirata.png)

# ¡Quiero ser pirata!

Les piratas nos damos unos acuerdos básicos de cuidados mutuos a través de
[nuestros códigos para compartir](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html),
que nos guían en la convivencia cotidiana y en el reconocimiento mutuo.

Somos un colectivo de personas y cyborgs que nos organizamos en base a
relaciones de afinidad, solidaridad, cuidados mutuos y afectividad. Esto no
significa que tengamos que estar de acuerdo en todo ni que pretendamos
homogeneizarnos en algún "estilo pirata". Significa, sobre todo que desechamos
las lógicas instrumentalistas y productivistas. Por sobre "los fines" o "los
productos" de ciertas acciones u objetivos políticos preferimos ejercitar otras
lógicas: de autocuidados, procesuales, de acompañamiento, de relacionamiento. Un
ejemplo de instrumentalización al que no adherimos podría ser poner sobre
nuestra salud y bienestar el cumplimiento de tal proyecto u objetivo, resultando
en "quemarnos entre compañeres". ("Quemarnos" como en "burn out", o sea estrés
crónico, sumado a los problemas relacionales entre nosotres que podría causar!).

Al mismo tiempo, como parte de esa lógica de cuidados, nos comprometemos a
acompañarnos en el tiempo que nos lleve descolonizarnos y asumir roles,
actitudes, tareas, gestos más empáticos entre todes, como una propuesta lo más
coherente posible con la transformación colectiva.

Esto quiere decir que el PIP está abierto para todes quienes se reconozcan en
estos acuerdos, pero no hay un procedimiento burocrático/ formal por el cual
alguien llega a ser pirata. Más bien llegamos a ser piratas cuando compartimos:

* Un acuerdo básico sobre [los códigos para compartir](https://utopia.partidopirata.com.ar/zines/codigos_para_compartir.html).

* Un compromiso para cumplir y hacer cumplir lo mejor que podamos estos cuidados
  colectivos.

* Un reconocimiento mutuo con otres piratas, basada en la afinidad, la
  afectividad y la empatía.
  
* Un posicionamiento en nuestras prácticas antiopresivas y de cuidados mutuos.

* La participación en una barca o proyectos piratas, con un compromiso sostenido
  en alguna regularidad.
  
Para posicionarnos como un colectivo antiopresivo y de cuidados, nos encontramos
en un proceso de compromiso y trabajo sostenido. Este proceso fue consensuado y
asumido tras un quiebre histórico, en el que se dieron una serie de discusiones
en torno a planteos trans/ feministas (sobre todo, venían siendo planteadas
discusiones de parte de varias compañeras) y alianzas con diferentes grupos del
"campo popular". Esto significa que seguimos desandando y repensando ciertas
prácticas, sobre todo las paternalistas, cis-patriarcales, man-sys-plainers.
(_Esperamos prontamente compartir un documento de nuestra memoria colectiva, en el que estamos trabajando_).
Sobre todo, porque para asumirnos como un colectivo
de estas características, creemos que es imprescindible poder dar cuenta de
comportamientos, posicionamientos y actitudes, pasadas y presentes, que no
cumplan o hayan cumplido con estos acuerdos.

## Qué hacemos

* Software y hardware libres ([¡y antipatriarcal!](https://partidopirata.com.ar/2019/11/02/declaracion-de-liberacion-del-software-libre/))
* (re)apropiación crítica de las técnicas de biopolíticas
* Movimientos de base o territoriales de justicia social
* Prácticas de cuidados antiopresivos
* Desobediencia cibernética
* Prácticas de cuidados digitales y autodefensa digital
* Transhacktivismos y hacktivismos LGTTTBIQA*
* Comunicación alternativa, comunitaria, popular
* Derecho a la comunicación
* Datos abiertos, democracia participativa
* Autodeterminación & #DoItOurselves
* Prácticas de cuidados mutuos y colectivos
* Artes digitales
* Cultura libre
* Editoriales libres, independientes y autogestivas
* [Sousveillance](https://es.wikipedia.org/wiki/Sousveillance)
* Estrategias de resistencia
* Ecosocialismo, permacultura y _low techs_
* Emancipación tecnológica y cognitiva
* Economías populares y solidarias

## Cómo acercarse

> ¡Mi nombre es Guybrush Threepwood y quiero ser pirata!

Si necesitás ponerte en contacto directo con nosotres, preferimos que nos
escribas a nuestra identidad colectiva
[René Montes](https://t.me/ReneMontes_bot).
De esta forma garantizamos respuesta, no
exponemos piratas y esquivamos referencias individuales. (Tené en cuenta que
quizás estemos teniendo problemas con la identidad. Si no te contestamos, volvé
a mandar tu mensaje mas tarde o en otro momento :)

Por correo: <contacto@partidopirata.com.ar>

En el Fediverso: <https://todon.nl/@pip>

En Telegram: <https://t.me/PartidoInterdimensionalPirata>

En Twitter: <https://twitter.com/PartidoPirataAr>

## Cómo nos organizamos

En el Partido Interdimensional Pirata tomamos decisiones por consenso y nos
organizamos alrededor de barcas, que son espacios auto-organizados alrededor de
temas, problemáticas, acciones, afinidades y proyectos.


## Cómo se toman decisiones

Les piratas tomamos decisiones por **consenso**. Esto quiere decir que
presentamos nuestras propuestas, las discutimos entre todes y llegamos a una
posición común. No quiere decir que todes tenemos que votar de la misma forma,
sino que al tomar una decisión estamos teniendo en cuenta las posiciones de
todes les piratas involucrades. El consenso es un proceso constante de una
acción, lo que nos permite actuar con flexibilidad, darnos cuenta de nuestros
errores y aciertos y considerar otras posiciones.

Usamos las ["Herramientas para la democracia directa"](https://utopia.partidopirata.com.ar/democracia_directa.html) como guía
y nuestra propia plataforma de toma de decisiones por consenso:
[Lumi](https://lumi.partidopirata.com.ar).

## Barcas

Las barcas son grupos de afinidad sobre temas específicos relacionados con
nuestros activismos. Para crear una barca solo es necesario el consenso y la
participación de tres piratas. Las barcas tienen autonomía para actuar pero
tienen el compromiso de funcionar de acuerdo a este documento y sostener los
códigos para compartir internamente y hacia las actividades que organicen
(talleres, encuentros presenciales, etc.). Es decir, deben reconocerse y ser
reconocidas como barcas piratas P)

Las barcas se comunican entre sí y colaboran en distintas acciones. Para no
pisarnos, pedimos colaboración de otras barcas cuando hacemos cosas que se
solapan.

Algunas barcas de las que podés participar son las siguientes... No son todas,
son las que están abiertas a través de chats grupales en Telegram y son públicas
porque cualquier interesade participa de sus encuentros, producción y
hacktividades.

* Utopías Piratas: Edición de libros y zines (pedir invitación).

* Barca Transfeminista

* [Barca LGBTTTIQA](tg://join?invite=VKi3NlglJ07VsA8T)

* [Varones Antipatriarcales](https://t.me/joinchat/DQyUbE_uo_NhJQ1F2vLbBw):
  Debate sobre deconstrucción cis-masculina y masculinidades, antipatriarcal!

* [Barca Platense](https://t.me/partido_pirata_la_plata): Piratas oriundes de la
  ciudad de La Plata.

* [LUMI](https://t.me/joinchat/BayXVRTDVFJpKqZdli-yiQ): Donde estamos
  desarrollando nuestra plataforma digital de toma de decisiones.

* Interdimensional: Punto de contacto para piratas perdidas en el espacio (pedir
  invitación).
  
* Barca Grog & Tor (autodefensa digital): Pedir indicaciones P)

* Barca Comunicación Pirata

* Barca Astillero de memes 

* Barca Infra 

* Barca P2P

* Barca Wiki

* [Barca Sindical Pirata](https://t.me/BarcaSindicalPirata): Sobre temas relacionados al mundo del trabajo. Bajo dependencia o independiente. (No quedan excluides les autogestionades).

* [Galponeo Técnico](https://t.me/PipGalponeoTecnico): Aca podés preguntar, aprender y compartir sobre temas más específicamente técnicos.

* Barca PyData: La barca es de análisis de datos. Son bienvenidos todo tipo de saberes y los conocimientos de otros lenguajes de programación. (Pedir invitación)

* [Barca de cine](https://t.me/joinchat/Uy_cirGRLwPUq8ao).

## Difusión

* [Vespertino Pirata](https://t.me/pipar_vesperino): Novedades del PIP en forma
  de mensajes unidireccionales, estos además se redireccionan para ser
  comentados y debatidos en el canal público.

* [Canal público](https://t.me/PartidoInterdimensionalPirata/): Ahí podrás
  saludar, esperar un rato y conversar con nosotres y, por sobre todo, con
  muches otres cyborgs. Te pedimos que no te presentes con nombre y apellido,
  dónde y cómo vivís, etc. Sino con un alias/nombre de usuarie que te
  identifique. Por cuestiones de cuidados personales y colectivos, queremos
  tener precaución respecto de la cantidad de información proporcionada de
  manera pública que pueda volvernos identificables, individualizables. Este
  canal es moderado a pulmón con nuestros esfuerzos colectivos, se baneará a
  discreción. Si te sentís incomode y querés advertir de comportamientos y
  comentarios, avisanos a nuestra identidad colectiva
  [René Montes](https://t.me/ReneMontes_bot).

* [Memazo](https://t.me/memazoreloaded): es como ver el cable interdimensional.

* [Fediverso](https://utopia.partidopirata.com.ar/zines/apostatemos_de_las_redes_asociales.html):
  Encontranos en el fediverso en <https://todon.nl/@pip>


## Cómo organizamos acciones

No tenemos representantes ni líderes. Sos bienvenide a sumarte a las barcas en
la medida de tus posibilidades. Si no podés cumplir con un compromiso que
asumiste, avisá a otres piratas con tiempo.

## Regla de les tres piratas

La _regla de les tres piratas_ es una medida "a ojo". Si tenés una idea, a otres
dos piratas les parece una buena idea y nadie se opone, quiere decir que hay un
consenso básico para empezar a actuar.

Acordate de esta canción:

> Si tu tienes muchas ganas de... \
> Si tu tienes la razón \
> y no hay oposición \
> no te quedes con las ganas de...

## Propuesta significa acción

Estamos acostumbrades a no tener la capacidad de actuar sin que alguien nos diga
qué es lo que tenemos que hacer y cómo. Les piratas promovemos el consenso para
saber que actuamos con el apoyo de otres y nos damos capacidad de acción
autónoma y colectiva. Si no sabemos, aprendemos juntes. Si se nos ocurre algo y
a otres les parece bien, lo llevamos a cabo en la medida de nuestras capacidades
y tiempos disponibles.

Sin embargo, hay que tener cuidado al hacer comentarios sobre las acciones
ajenas, porque muchas veces ponerse a opinar sin hacer puede quitarle el empuje
a la acción en marcha, a esto lo llamamos "galponeo".

## Ni une pirata sole

Les piratas nos consideramos en estado de cuidado colectivo: Prestamos atención
a cómo están les compañeres, a cómo nos sentimos y nos hacen sentir y trabajamos
desde los afectos. Es importante que ningune se "queme" haciendo tareas sole,
que pase el punto en que una tarea auto-asumida deja de ser disfrutable y pase a
ser una carga.

## Piratas sombra

Les piratas sombra se ofrecen voluntariamente a ayudar a une pirata nueva a
participar. Si querés participar y necesitás una mano para ubicarte, pedí tus
sombras P)

## Preguntas frecuentes

### ¿Por qué son un "Partido"?

Porque somos parte de un movimiento político y aunque no quiere decir que nos
posicionemos en su vanguardia, nos agrupamos para actuar colectivamente. Que
haya un "partido" en el nombre no quiere decir que nos vayamos a presentar a
elecciones.

Vamos a desarrollar más este punto cuando publiquemos la memoria del PIP. En
general, preferimos decir que somos un movimiento más que un partido en términos
tradicionales, vanguardistas, electoralistas, si bien nacimos como la rama local
en Argentina del Partido Pirata internacional. En este momento, seguimos en
contacto y funcionamos articuladamente como red --muchas veces-- con otros
partidos piratas en el mundo, en especial latinoamérica. Esto no significa que
estemos de acuerdo en todo. Cada partido funciona en forma independiente y
maneja su propia agenda. Más allá de la raíz histórica, es posible que haya más
de algún acuerdo o visión compartida.

### ¿Por qué "Interdimensional"?

Para que no se tomen en serio nuestro nombre, sino cómo nos organizamos y las
cosas que hacemos. Por otro lado, actuamos desde distintas e infinitas
dimensiones.

### ¿Por qué "Piratas"?

Al apropiarnos del "insulto", demostramos con nuestras prácticas que podemos
liberar la cultura, la información, los conocimientos y los saberes para
socializarlos entre todes. [¡Copiar no es robar!](https://invidio.us/search?q=Copiar+no+es+robar)

<!-- el link está asi xq no encontré la version en castellano y me gusta q esté
subido muchas veces ese video es lo masss <3 -->
