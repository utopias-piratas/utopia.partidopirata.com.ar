---
author: Partido Interdimensional Pirata
title: Que no nos acosen por Facebook
toc: false
layout: post
cover: "assets/covers/single/que_no_nos_acosen_por_facebook.png"
signature: 0
---

# QUE NO NOS ACOSEN POR FACEBOOK

YA SABEMOS QUE **FACEBOOK GANA MILES DE MILLONES DE DÓLARES** CON TODO EL
CONTENIDO QUE SUBIMOS A SU PLATAFORMA, POR ESO LAS PIRATAS DECIMOS QUE
**TRABAJAMOS PARA FACEBOOK** P), PERO ADEMÁS, POR LA CANTIDAD DE DATOS QUE
LE PROPORCIONAMOS Y PORQUE FACEBOOK NO LOS CUIDA, **LOS MACHIRULOS Y LAS
FUERZAS REPRESIVAS LOS USAN PARA HOSTIGARNOS O VIGILARNOS**.

NO VAMOS A DECIR QUE TENEMOS QUE CERRAR NUESTRAS CUENTAS (...O SÍ), ASÍ
QUE COMPARTIMOS UNA GUÍA PARA QUE NUESTRAS CUENTAS SEAN **UN POCO MÁS
PRIVADAS**.

ESTO NO ASEGURA QUE NO SE PUEDA TENER ACCESO A LAS COSAS QUE PUBLICAMOS
EN FACEBOOK, SOLO LES DIFICULTA EL TRABAJO. ESPECIALMENTE **LAS FUERZAS
REPRESIVAS TIENEN ACCESO DIRECTO A LOS DATOS** DE FACEBOOK Y OTRAS
PLATAFORMAS CAPITALISTAS.

## PRIVACIDAD

EN LA SECCIÓN **CONFIGURACIÓN > PRIVACIDAD**, AJUSTÁ LAS SIGUIENTES OPCIONES:

* ¿QUIÉN PUEDE VER LAS PUBLICACIONES QUE HAGAS A PARTIR DE AHORA? **>
  AMIGXS**
* ¿QUIERES LIMITAR LXS DESTINATARIXS DE LAS PUBLICACIONES QUE
  COMPARTISTE CON LOS AMIGXS DE TUS AMIGXS O QUE HICISTE PÚBLICAS? **> SÍ**
* ¿QUIÉN PUEDE ENVIARTE SOLICITUDES DE AMISTAD? **> AMIGXS DE AMIGXS**
* ¿QUIÉN PUEDE VER TU LISTA DE AMIGXS? **> SOLO YO**
* ¿QUIÉN PUEDE BUSCARTE CON LA DIRECCIÓN DE CORREO ELECTRÓNICO QUE
  PROPORCIONASTE? **> AMIGXS**
* ¿QUIÉN PUEDE BUSCARTE CON EL NÚMERO DE TELÉFONO QUE PROPORCIONASTE? **>
  AMIGXS**
* ¿QUIERES QUE LOS MOTORES DE BÚSQUEDA FUERA DE FACEBOOK ENLACEN A TU
  PERFIL? **> NO**
* DESACTIVÁ EL RECONOCIMIENTO FACIAL


## UBICACIÓN:

* VER HISTORIAL DE UBICACIONES **> ...** (MENÚ DE PUNTOS VERTICALES) **>
  ELIMINAR TODO EL HISTORIAL DE UBICACIONES**
* CONFIGURACIÓN **>** DESACTIVAR HISTORIAL **> DESACTIVAR SERVICIOS DE
  UBICACIÓN**

## BIOGRAFÍA Y ETIQUETADO:

* ¿QUIÉN PUEDE PUBLICAR EN TU BIOGRAFÍA? **> AMIGXS/YO**
* ¿QUIÉN PUEDE VER LO QUE OTRXS PUBLICAN EN TU BIOGRAFÍA? **>
  AMIGXS/YO**
* ¿QUIÉN PUEDE VER LAS PUBLICACIONES EN LAS QUE TE ETIQUETAN EN TU
  BIOGRAFÍA? **> AMIGXS/YO**
* ¿QUIERES REVISAR LAS PUBLICACIONES EN LAS QUE TE ETIQUETA ANTES DE QUE
  APAREZCAN EN TU BIOGRAFÍA? **> ACTIVADO**
* ¿QUIERES REVISAR LAS ETIQUETAS QUE LAS PERSONAS AGREGAN ANTES DE QUE
  APAREZCAN EN TU BIOGRAFÍA? **> ACTIVADO**


## SEGURIDAD E INICIO DE SESIÓN:

* DÓNDE INICIASTE SESIÓN **> BORRÁ TODOS LOS QUE NO CONOZCAS O HAYAS
  DEJADO DE USAR**
* CONTRASEÑAS **>** ES RECOMENDABLE USAR **UNA FRASE QUE SEPAMOS DE
  MEMORIA**, PERO NO LE CONTEMOS A NADIE. **3 O 4 PALABRAS AL AZAR** O,
  MEJOR, USAR UN **GESTOR DE CONTRASEÑAS** COMO KEEPASS XD.
* DESACTIVÁ EL INICIO DE SESIÓN CON FOTO
* ACTIVÁ LA AUTENTICACIÓN EN DOS PASOS. PODEMOS USAR CELULAR Y
  CONTRASEÑA.
* CONTRASEÑAS PARA APLICACIONES **>** SI ACTIVAMOS ESTA OPCIÓN PODEMOS
  GENERAR CONTRASEÑAS ESPECÍFICAS PARA OTRAS APPS EN LAS QUE NOS
  REGISTRAMOS, EN VEZ DE USAR EL INICIO DE SESIÓN AUTOMÁTICO CON
  FACEBOOK. ESTAS CONTRASEÑAS LUEGO SE PUEDEN REVOCAR, **LIMITANDO EL
  ACCESO QUE TIENEN LAS APPS** A NUESTRA CUENTA.
* RECIBIR ALERTAS SOBRE INICIOS DE SESIÓN NO RECONOCIDOS **> ACTIVAR**
* ELEGIR DE 3 A 5 AMIGXS PARA CONTACTAR EN CASO DE QUE PIERDAS EL ACCESO
  A TU CUENTA **>** SI NO QUEREMOS QUE FB SEPA QUIÉNES SON, QUIZÁS LO
  MEJOR SERÍA DARLES LA CONTRASEÑA EN PERSONA.


**TINFOIL FOR FACEBOOK** ES UNA APLICACIÓN QUE PODEMOS USAR EN EL
CELULAR PARA NAVEGAR EN EL SITIO WEB MÓVIL DE FACEBOOK.  COMO NO TIENE
ACCESO A LOS CONTACTOS Y OTROS DATOS, **MINIMIZAMOS LOS DATOS** QUE NOS
PUEDEN EXTRAER.

AL OCUPAR MENOS ESPACIO Y SER **MUCHO MÁS LIVIANA**, NO NOS CONSUME
TANTOS DATOS.

PODÉS DESCARGAR LA APLICACIÓN DESDE F-DROID O GOOGLE PLAY.

## ¡NOS VEMOS EN EL FEDIVERSO!

SI ESTÁS PODRIDX Y NO QUERÉS SABER MÁS NADA, PODÉS CERRAR TUS
CUENTAS Y VENIRTE AL FEDIVERSO.

EL FEDIVERSO ES UNA RED DE PLATAFORMAS DE COMUNICACIÓN QUE SE
INTERCONECTAN ENTRE SÍ, SE FEDERAN. SI PARA COMUNICARTE CON ALGUIEN EN
FACEBOOK HAY QUE TENER TAMBIÉN UNA CUENTA EN FACEBOOK Y LO MISMO PARA
TWITTER Y ASÍ PARA TODAS LAS PLATAFORMAS, EN EL FEDIVERSO SÓLO ES
NECESARIO ELEGIR UN NODO, EL QUE NOS CAIGA MÁS SIMPÁTICO, EL QUE NOS
PAREZCA MÁS CONFIABLE Y A TRAVÉS DE ESTE, YA NOS PODEMOS COMUNICAR CON
EL RESTO.

LAS PIRATAS ESTAMOS EN TODON.NL: <https://todon.nl>

**Partido Interdimensional Pirata**
