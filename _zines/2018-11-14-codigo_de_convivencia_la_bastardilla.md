---
title: Código de convivencia
author: La Bastardilla
signature: 0
toc: false
cover: assets/covers/single/codigo_de_convivencia_la_bastardilla_texto.png
layout: post
---

# Código de convivencia

## La Bastardilla

La Bastardilla es una feria transfeminista, disidente y autogestiva, nos
convocamos desde un trabajo horizontal, y apuntamos a una economía
social y solidaria en la que circulen ideas, ilustraciones, fanzines,
papeles y libros accesibles a todes en términos de temática y a todes en
términos económicos y/o de intercambio...

Este espacio está en construcción, publicamos y nos publicamos porque
creemos que de esta manera alimentamos/avivamos un circuito lleno de
energía, una apuesta de autorxs, librerxs, ilustradorxs… hacia lectorxs,
amigxs, y todxs aquellxs que apoyan la circulación alternativa.

Apoyamos la circulación de textos más allá de la propiedad intelectual,
consideramos que todx autorx crea a partir de creaciones ajenas.
Promovemos la reproducción de libros como una herramienta más para la
creación de nuevas ideas y la cultura libre como forma de asegurar que
nuestras obras puedan ser copiadas y reelaboradas, compartidas y
traducidas.

También tenemos la perspectiva de generar encuentros fuera de la venta
de libros, espacios para intercambiar saberes, compartir datas o
potenciar redes.

Cualquier actitud machista, transodiante, gordxodiante,
lesbobiputxodiante NO SERÁ TOLERADA.

## Introducción

Este es un código que busca aportar un marco de consenso para garantizar
la asistencia, permanencia y comodidad de todas las personas que habiten
La Bastardilla. Para esto, fija un piso de conductas deseables,
aceptables, indeseables y/o intolerables en el marco de la feria y todas
sus actividades. Podés usarlo sin cambios o modificarlo para adaptarlo a
tus actividades. Este código está en permanente mutación colectiva, y se
alimenta, copia e inspira de las siguientes fuentes:

* <https://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy>

* <https://trans-code.org/code-of-conduct/>

* <https://openhardware.science/logistics/gosh-code-of-conduct/>

* <https://hypatiasoftware.org/code-of-conduct/>

* <https://wiki.partidopirata.com.ar/c%C3%B3digos-para-compartir/>

Procuramos mantener y fomentar un espacio que invite y logre la
participación de cada vez más personas, en toda su diversidad.

## Puntos importantes para garantizar que nuestra actividad no resulte expulsiva

**Escucharnos a todes y entre todes en un clima seguro**

* Para la escucha activa, preferimos preguntar primero, en lugar de
  emitir juicios.

* Hacer silencio a veces es la condición para que otres puedan animarse
  a hablar. Escuchar es un ejercicio que requiere práctica.

* Nos interesa lo que todes tengan para decir. Por lo tanto, si estás
  más predispueste en el ejercicio de participar, hablar, opinar, tené
  en cuenta que quizás haya otres que no lo estan tanto, por eso es
  importante ceder el espacio para que les demas, si quieren, puedan
  tomarlo. ¡Pero recordá que incentivar no es lo mismo que presionar!

* Le damos tiempo y espacio a todes para que puedan opinar / participar

* para sumar al clima de respeto ajustamos nuestro comportamiento
  apropiadamente cuando nos lo pidan.

* Es una falta de respeto repetir un comportamiento dañino que ya se
  identificó como tal. Puede incomodar, lastimar y expulsar a otres, por
  lo que preferimos llamar la atención sobre dichos comportamientos
  todas las veces que sea necesario y tolerable.

* Evitamos esto cada une de nosotres y ayudamos a otres a darse cuenta
  cuando lo están haciendo.

* En los casos en los que los llamados de atención resultan
  insuficientes, preferimos apelar a este código para sostener la
  convivencia.

* Una manera en la que los espacios pueden y suelen ser expulsivos es
  mediante actitudes que no contemplan la diversidad de saberes, formas
  de habitar, vivenciar, participar y dialogar. So pretexto de incluir
  tecnicismos y/o diálogos cerrados, muches compañeres quedan al margen
  de lo que está sucediendo, muchas veces, sin que nadie tenga la mínima
  delicadeza de verificar que todes estén siguiendo la conversación.
  Otra forma es a partir de las capacidades físicas, motrices,
  auditivas, visuales, etc… Tratamos de que las actividades contemplen
  la participación de las personas más allá de sus características
  físicas o su condición de salud. Lo mismo con respecto a la edad de
  las personas, muchas veces vemos que ancianxs y niñes no encuentran
  cabida en actividades similares a La Bastardilla, insistimos en que
  este espacio invite a la participación de todes mas allá de su edad.

Recomendamos fervientemente estar atentes a estas dinámicas para poder
evitarlas y, eventualmente, revertirlas.

* La otra cara de la situación anterior es el "mansplaining": un tipo
  poniéndose en el lugar de la autoridad del saber para (sobre-)
  explicar todo a otre, de manera paternalista y subestimante, sin tener
  en cuenta lo que le otre quiere o no escuchar, decir, necesita, lo que
  sabe o hace, etc.

* Creemos que no hace falta ser "una voz autorizada" para opinar y
  participar. El conocimiento y la cultura se comparten entre todes.

* No damos por sentado que la persona con la que estamos interactuando
  comparte gustos, creencias, pertenencias de clase, género, orientación
  sexo-afectiva, etc.  Podemos ser violentes si hacemos una lectura
  equivocada de les otres Recomendamos siempre preguntar de manera
  respetuosa y evitar comentarios o chistes que puedan herir a les demás

* Usamos lenguaje amable e inclusivo y mostramos conductas amables e
  inclusivas.

* Respetamos los diferentes puntos de vista, experiencias, creencias,
  saberes, formas de habitar, cuerpas, etc., y lo tenemos en cuenta
  cuando estamos abriendo la participación.

* Aceptamos las críticas. En especial las constructivas ;)

* Nos enfocamos en lo que es mejor para la actividad, sin por ello
  perder de vista la calidez, el respeto y la diversidad entre cada une
  de nosotres.

* Mostramos empatía con les otres. Queremos comunicarnos y compartir.

* Respetamos los límites que establecen otras personas (espacio
  personal, contacto fisico, ganas de interactuar, no querer dar datos
  de contacto o ser fotografiades, etc.)

* ¡Queremos y (creemos) en sumar nuevas aliades!

## Consentimiento para documentar o compartir en medios

* Si vas a filmar o sacar fotos, comentalo para que las personas estén
  al tanto y puedan dar o no su consentimiento.

* Si hay menores, consultalo con su familia y/o responsable.


## Nuestro compromiso contra el acoso

En el interés de fomentar una comunidad abierta, diversa, y acogedora,
nosotres nos comprometemos a hacer de la participación en La Bastardilla
una experiencia libre de acoso para todes, independientemente de la
edad, diversidad corporal, capacidades, neuro-diversidad, etnia,
identidad, género orientación sexo-afectiva, expresión de género,
nacionalidad, apariencia física, raza, religión, identidad y creencia.


### Ejemplos de comportamiento inaceptable por parte de participantes:

* Comentarios ofensivos relacionados con el/los género/s, la indentidad,
  expresión de género, orientación sexual, capacidades, neuro
  atipicalidad, la apariencia física, el tamaño corporal, creencia, raza
  y religión.

* Comentarios ofensivos relacionados con las elecciones y las prácticas
  de estilo de vida de una persona, incluidas, entre otras, las
  relacionadas con alimentos, salud, estética, crianza de les hijes,
  consumos y trabajo.

* Comentarios insultantes o despectivos (*trolling*) y ataques
  personales o políticos (chicanas).

* Dar por sentado el género de las demás personas.  En caso de duda,
  preguntá educadamente por el/ los pronombre/s con los que se
  identifican tus interlocutorxs. No uses nombres con los que las
  personas no se identifiquen, usá el nombre o apodo que hayan elegido.

* Comentarios, imágenes o comportamientos sexuales innecesarios o fuera
  de lugar en espacios en los que no son apropiados.

* Contacto físico sin consentimiento o reiterado tras un pedido de cese.

* Amenazas contra otras personas.

* Incitación a la violencia contra otra persona, que también incluye
  alentar a una persona a suicidarse o autolesionarse.

* Intimidación deliberada.

* Acechar (**stalkear**) y/o perseguir.

* Acosar fotografiando o grabando sin consentimiento, incluido también
  compartir información personal sobre alguien sin contar con su
  consentimiento.

* Interrumpir constantemente en una charla.

* Hacer comentarios sexuales inapropiados.

* Patrones de contacto social inapropiados, como por ejemplo
  pedir/suponer niveles de intimidad no consensuados con les demás.

* Seguir tratando de entablar conversación con una persona cuando se te
  pidió que no lo hagas.

* Divulgar deliberadamente cualquier aspecto de la identidad de una
  persona sin su consentimiento, excepto que sea necesario para proteger
  a otras personas de abuso intencional.

* Hacer pública una conversación privada de cualquier tipo.

* Otros tipos de conducta que pudieran considerarse inapropiadas en un
  entorno de camaradería.

* Reiteración de actitudes que les participantes señalen como ofensivas
  o violatorias de este código.

## Consecuencias

* Se espera que la persona a la que se la haya pedido que cese un
  comportamiento que infringe este código acate el pedido de forma
  inmediata, incluso si no está de acuerdo con este.

* Les organizadores para tal fin pueden tomar cualquier acción que
  juzguen necesaria y adecuada, incluido expulsar a la persona de la
  actividad sin advertencia. Esta decisión la toman les organizadores en
  consenso y se reserva para casos  que comprometan la continuidad de la
  actividad o bien la posibilidad de permanencia de otras participantes
  sin sentirse agraviadas o amenazadas.

* Les organizadores reservan el derecho a prohibir la asistencia a
  cualquier actividad futura.

Como mencionamos antes, este código está en permanente mutación
colectiva. El objetivo principal es generar un ambiente inclusivo y no
expulsivo, un ambiente transparente y abierto en el que no haya
escalones faltantes ("el escalón que falta en la escalera y todo el
mundo sabe y avisa pero nadie se quiere hacer cargo"). Es importante que
se adapte a las actividades y se nutra de las constribuciones de les
asistentes.  Recibir tus comentarios y aportes nos ayuda a cumplir con
ese objetivo.

## ¡Sigamos en contacto!

Si pasaste por alguna situación que quieras compartir --te hayas animado
o no a decirlo en el momento--, podés ponerte en contacto con nosotres.

Con respecto a quejas o avisos acerca de situaciones de violencia,
acoso, abuso o repetición de conductas que se advirtieron como
intolerables, tomamos la responsabilidad de tenerlas en cuenta y
trabajar en ellas para que el resultado se adecúe al espíritu de
colectiva que elegimos y describimos aquí.  Si bien consideramos que las
prácticas punitivistas no nos caben, nuestra decisión explícita es
escuchar a la persona que se manifiesta como violentada y acompañarla de
forma tal de poder garantizar que no tenga que volver a vivir una
situación de acoso y/o violencia.

**labastardilla@riseup.net**
