---
layout: post
cover: assets/covers/donde_esta_el_feminismo_en_el_ciberfeminismo.png
contra: assets/covers/donde_esta_el_feminismo_en_el_ciberfeminismo_contra.png
title: ¿Dónde está el feminismo en el ciberfeminismo?
author: Faith Wilding
toc: false
signature: 0
pages: 2-27
references:
  - type: article-journal
    id: bassett-1997
    author:
      - family: Bassett
        given: Caroline
    issued:
      year: 1997
    title: "With a Little Help from Our (New) Friends?"
    container-title: mute
    page: 46-49
  - type: chapter
    id: braidotti-2004
    author:
      - family: Braidotti
        given: Rosi
    issued:
      year: 2004
    title: "El ciberfeminismo con una diferencia"
    container-title: "Feminismo, diferencia sexual y subjetividad nómade"
    editor:
      - family: Fischer Pfeiffer
        given: Amalia
    publisher-place: Barcelona
    publisher: Gedisa
  - type: book
    id: penn-1997
    author:
      - family: Penn
        given: Shana
    issued:
      year: 1997
    title: "The Women's Guide to the Wired World"
    publisher-place: Nueva York
    publisher: Feminist Press
  - type: article-journal
    id: plant-1997
    author:
      - family: Plant
        given: Sadie
    issued:
      year: 1997
    title: "Zeros + Ones: Digital Women + The New Technocultures"
    publisher-place: Nueva York
    publisher: Doubleday
    volume: 37
---

# ¿Dónde está el feminismo en el ciberfeminismo?

## Contra la definición

La cuestión de cómo definir el ciberfeminismo yace en el centro de las
posturas contemporáneas --a menudo contradictorias-- de las mujeres que
trabajan con las nuevas tecnologías y la política feminista. Por
ejemplo, la postura de Sadie Plant sobre el ciberfeminismo se ha
identificado como "una insurrección absolutamente post-humana; la
revuelta de un sistema emergente que incluye a las mujeres y las
computadoras, contra la visión del mundo y de la realidad material de un
patriarcado que aún busca subyugarlas. Es una alianza de los bienes
contra sus amos, una alianza de mujeres y máquinas" [@bassett-1997, pp.
46-49]. Esta visión utópica de la revuelta y la fusión entre mujer y
máquina es también evidente en el Manifiesto Ciberfeminista la Matriz
VNS para el siglo XXI: "somos el virus del  nuevo  desorden
mundial/rompemos  lo  simbólico  desde  dentro/saboteadoras del
mainframe del gran padre/el clítoris es una línea directa a la
matriz"[^1]. Rosi Braidotti ofrece otra postura en este debate: [...] el
ciberfeminismo necesita fomentar una cultura de júbilo y afirmación.
[...] Hoy las mujeres deben emprender la danza en el ciberespacio,
aunque sólo sea para asegurarse de que las palancas de mando de los
vaqueros del ciberespacio no reproduzcan la falicidad unívoca bajo la
máscara de la multiplicidad [@braidotti-2004].

[^1]: Página web de la Matriz VNS: <http://sysx.apana.org.au/artists/vns/>

El boletín de prensa emitido en las discusiones ciberfeministas de
Kassel declaraba que: "La Primera INTERNACIONAL CIBERFEMINISTA se
escabulle por las trampas de la definición con diferentes actitudes
hacia el arte, la cultura, la teoría, la política, la comunicación y la
tecnología: el terreno de internet". Extrañamente, lo que emergió de
estas discusiones fue el intento de definir el ciberfeminismo por medio
del rechazo, evidente no sólo en la intensidad de los argumentos, sino
también en las 100 antítesis ahí ideadas; por ejemplo: "cyberfeminism is
not a fashion statement / sajbrfeminizm nije usamljen / cyberfeminism is
not ideology / cyberfeminismus ist keine theorie / el ciberfeminismo no
es una frontera /..."[^2]. Aun así, las razones expresadas por aquellas
que rehusaron definir el ciberfeminismo --aunque se llamen
ciberfeministas-- indican una profunda ambivalencia en la relación de
muchas mujeres conectadas, con lo que perciben como una historia, teoría
y práctica feministas pasadas y monumentales. Merecen un examen más
cercano tres manifestaciones principales de esta ambivalencia y su
relevancia con respecto a las condiciones contemporáneas de las mujeres
inmersas en la tecnología.

[^2]: Las 100 anti-tesis completas se recogen en un artículo en este
  mismo dossier. Hemos mantenido el plurilingüismo porque es
  significativo.


## 1. Repudio del feminismo de "la vieja escuela" (años setenta)

Según este argumento, el feminismo de "la vieja escuela" (años setenta)
se caracteriza como monumental, a menudo restrictivo (políticamente
correcto), induce a la culpa, es esencialista, anti-tecnología,
anti-sexo y no es relevante para las condiciones de la mujer en las
nuevas tecnologías (a juzgar por las discusiones de Kassel, esta
concepción es común en Estados Unidos y Europa occidental).
Irónicamente, en la práctica real, el ciberfeminismo ya ha adoptado
muchas de las estrategias de los movimientos feministas de vanguardia,
incluyendo el separatismo estratégico (listas, grupos de autoayuda,
chats, redes, todo ello sólo para mujeres y entrenamiento tecnológico
entre mujeres), teorías y análisis feministas culturales, sociales y de
lenguaje, creación de nuevas imágenes de la mujer en la Red para
contrarrestar los estereotipos sexistas desenfrenados (avatares
feministas, cyborgs, fusión genérica), crítica feminista de la Red,
esencialismo estratégico y similares. El repudio del feminismo histórico
es problemático porque tira todo por la borda y se alinea incómodamente
con los miedos populares, los estereotipos y los conceptos erróneos del
feminismo.  ¿Por qué tantas mujeres (y hombres) jóvenes en Estados
Unidos (y Europa) saben tan poco incluso de las historias muy recientes
de las mujeres, por no hablar de movimientos y filosofías feministas del
pasado?  Es tentador señalar a los sistemas e instituciones educativas
que aún tratan las historias de la mujer y de lo étnico racial y a las
poblaciones marginadas como subsidiarias de la "historia" regular,
relegándolas a cursos o departamentos especializados. Pero los problemas
van más allá.  El trabajo político de construir un movimiento es una
experiencia que debe ser reaprendida por cada generación, y necesita la
ayuda de practicantes experimentadas. La lucha por mantener vivas hoy en
día las prácticas e historias de resistencia es más dura en vista de una
cultura de la comodidad que prospera a partir de la novedad, la
velocidad, la obsolescencia, la evanescencia, la virtualidad, la
simulación y las promesas utópicas de la tecnología. La cultura de la
comodidad es siempre joven y hace parecer remoto y mítico incluso al
pasado reciente. Mientras que las mujeres jóvenes apenas están
ingresando en la economía tecnológica, muchas feministas mayores se
sienten inseguras acerca de cómo conectarse con los asuntos de las
mujeres que trabajan con las nuevas tecnologías y sobre cómo adaptar las
estrategias feministas a las condiciones de la nueva cultura de la
información. Entonces, el problema para el ciberfeminismo es cómo
incorporar las lecciones de historia en una práctica feminista activista
que sea adecuada para tratar los asuntos de las mujeres en una cultura
tecnológica.

Con seguridad, el problema de perder el conocimiento histórico y la
conexión activa con movimientos radicales del pasado no se limita al
feminismo: es endémica a los movimientos de izquierda en general. Al
abogar por la importancia de conocer la historia, no le estoy rindiendo
un homenaje nostálgico a glorias pasadas. Si las ciberfeministas quieren
evitar cometer los errores de feministas anteriores, deben comprender la
historia de la lucha feminista. Y si han de expandir su influencia en la
Red y negociar asuntos de diferencia a través de fronteras
generacionales, económicas, educacionales, raciales, nacionales y de
experiencia, deben buscar coaliciones y alianzas con diversos grupos de
mujeres involucradas en el circuito integrado de las tecnologías
globales. Al mismo tiempo, la familiaridad con los estudios
poscoloniales y con las historias de la dominación imperialista y
colonialista --y la resistencia a ella-- son igualmente importantes para
una práctica informada de la política ciberfeminista.

## 2. Cybergrrlismo[^3]

A juzgar por una búsqueda rápida en la red, una de las rebeliones
populares feministas practicadas en la actualidad por las mujeres en la
Red es el cybergrrl_ism en todas sus variaciones: _webgrrls_, _riot
grrls_, _guerrilla grrls_, _bad grrls_, etc. Como han señalado Rosi
Braidotti (2004) y otras, el trabajo a menudo irónico, paródico,
divertido, apasionado, enojado o agresivo de muchos de estos grupos
recientes de _grrls_ es una manifestación importante de nuevas
representaciones feministas subjetivas y culturales en el ciberespacio.
Actualmente hay una gran gama de articulaciones de prácticas feministas
y protofeministas en estos grupos diversos, que van desde las listas de
correo a las que "cualquier mujer se puede unir", hasta grupos de
ciencia ficción, ciberpunk y pornografía; proyectos antidiscriminación;
exhibicionismo sexual; experimentación transgenérica; separatismo
lésbico; autoyuda médica; autopromoción artística; servicios de empleo y
citas amorosas; y pura y simple conversación. El cybergrrlismo parece
generalmente suscribirse a una cierta cantidad de utopismo cibernético,
una actitud de "todo lo que quieras ser y hacer en el ciberespacio está
bien". A pesar de las quejas contra los hombres en general, que
impregnan algunas de las discusiones y sitios, la mayoría de cybergrrls
no parecen estar interesadas en involucrarse en una crítica política de
la postura de las mujeres en la Red y, en cambio, adoptan la actitud un
tanto antiteórica que parece prevalecer en la actualidad; prefieren
seguir adelante para expresar sus ideas directamente en su arte y sus
prácticas interactivas.

[^3]: Al omitir la i y agregar una r a _girl_ (chica) y convertirla en
  _grrl_, el término juega con la onomatopeya del gruñido, _grr_,
  dándole así una connotación de enfado o enojo, de agresividad (nota de
  la traducción).


Mientras que las cybergrrls se inspiran en ocasiones --consciente o
inconscientemente-- en los análisis feministas de las reproducciones de
la mujer en los medios masivos de comunicación --y en las estrategias y
trabajos de muchas artistas feministas–-, también a veces sin pensarlo
se apropian de y recirculan imágenes sexistas y estereotipadas de
mujeres de los medios populares --la puta pechugona, la cyborg
hipersexuada y las mujeres de las publicidades de _tupperware_ de los
años cincuenta son las favoritas-- sin ningún análisis ni
recontextualización crítica. La creación de imágenes feministas más
positivas y complejas que rompan los códigos "generados" prevalecientes
en la Red (y en los medios populares) necesita muchas cabezas
inteligentes y hay investigaciones feministas muy sugerentes, que van de
lxs cyborgs monstruosos de Haraway, la performatividad fluida de género
de Judith Butler, a los géneros recombinantes de Octavia Butler. Todos
los tipos de seres híbridos pueden desestabilizar los antiguos binomios
masculino/femenino.

Las trayectorias de estilo cybergrrls son importantes como vectores de
investigación, búsqueda, invención y afirmación. Pero no pueden
reemplazar el trabajo duro que se necesita para identificar y cambiar
las estructuras, contenidos y efectos "generados" de las nuevas
tecnologías de la mujer a nivel mundial. Si es cierto, como argumenta
Sadie Plant, que "las mujeres no sólo han tenido un papel menor que
representar en el surgimiento de las máquinas digitales... las mujeres
han sido las simuladoras, emsambladoras y programadoras de las máquinas
digitales" [@plant-1997] entonces, ¿por qué hay tan pocas mujeres en
posiciones visibles de liderazgo en el mundo electrónico? ¿Por qué son
las mujeres un porcentaje ínfimo de les programadores, diseñadores de
software, analistas de sistemas y _hackers_, mientras que son la mayoría
de trabajadoras de teletipos, ensambladoras e instaladoras de chips y
teleoperadoras no calificadas que mantienen en operación los datos
globales y los bancos de datos? ¿Por qué es aún la percepción popular
que las mujeres son tecnofóbicas? Tristemente, la lección de Ada
Lovelace es que aunque las mujeres han hecho contribuciones importantes
a la invención de las computadoras y la programación informática, esto
no ha cambiado la percepción –-o la realidad–- de la condición de las
mujeres en las nuevas tecnologías. Ser niñas malas en internet no va a
enfrentar por sí mismo al statu quo, aunque podría ofrecer momentos
refrescantes de delirio iconoclasta. Pero si la energía y la inventiva
de las chicas se aparejara con una práctica y una teoría política
comprometidas... ¡imagínense!

¡Imaginen a las teóricas feministas haciendo equipo con descaradas y
astutas ciberartistas para visualizar nuevas representaciones feministas
de cuerpos, lenguajes y subjetividades en el ciberespacio! Actualmente
hay poca colaboración (en Estados Unidos) entre teóricas académicas
feministas, artistas feministas y cultura popular feminista en la Red.
¿Qué pasaría si estos grupos trabajaran juntos para visualizar e
interpretar nuevas teorías y hacerlas circular en formas populares
accesibles? Imaginen usar redes electrónicas existentes para vincular
grupos diversos de usuarias de computadoras (incluyendo teletrabajadoras
e introductoras de datos) en un intercambio de información sobre sus
condiciones diarias de trabajo y sus vidas en la Red. Imaginen usar esta
red de información como una acción base para tratar asuntos de
trabajadoras digitales en la reestructuración global del trabajo. Tales
proyectos podrían interconectar las aspiraciones tanto utópicas como
políticas del ciberfeminismo.

## 3. Utopismo cibernético

Como ya hice notar en un ensayo anterior sobre la condición política del
ciberfeminismo, hay mucho que decir para poder considerarlo una
prometedora nueva ola de práctica feminista que pueda luchar por los
territorios tecnológicamente complejos y trazar nuevos terrenos para las
mujeres[^4]. No obstante, hay una tendencia entre muchas ciberfeministas
a albergar expectativas tecnoutópicas en donde los nuevos medios
electrónicos ofrecerán a las mujeres un empezar de cero para crear
nuevos lenguajes, programas, plataformas, imágenes, identidades fluidas
y definiciones multisubjetivas en el ciberespacio; de que las mujeres en
efecto pueden recodificar, rediseñar y reprogramar la tecnología de la
información para ayudar a cambiar sus condiciones de existencia. Este
utopismo cibernético declara que el ciberespacio es un espacio libre
donde el género no importa –-puedes ser lo que quieras sin importar tu
"verdadera" edad, sexo, etnia o posición económica–- y rechaza una
postura de sujetes fijes. En otras palabras, el ciberespacio se
considera como una arena inherentemente libre de las mismas viejas
relaciones y luchas de género. Sin embargo, es muy importante reconocer
que los nuevos medios existen dentro de un marco social ya establecido
en sus prácticas e insertado en ambientes económicos, políticos y
culturales que aún son profundamente sexistas y racistas. Contrariamente
a los sueños de muchas utopistas cibernéticas, la Red no borra
automáticamente las jerarquías por medio de los libres intercambios de
información a través de fronteras.  De igual modo, la Red no es una
utopía de no-género; ya está socialmente inscrita con respecto a los
cuerpos, el género, la edad, la economía, la clase social y la etnia. A
pesar de las indisputablemente innovadoras contribuciones de las mujeres
a la invención y el desarrollo de la tecnología computacional, la
Internet actual es un territorio en disputa que se originó
históricamente como un sistema al servicio de las tecnologías de guerra
y que actualmente es parte de las instituciones machistas.

[^4]: Faith Wilding y _Critical Art Ensemble_, "Notes on the Political
  Condition of Cyberfeminism".  <http:// mailer.fsu.edu/~sbarnes> [Notas
  sobre la condición política del ciberfeminismo].

Cualquier posibilidad nueva imaginada dentro de la Red debe primero
reconocer y tomar plena consideración de las implicaciones de sus
formaciones fundacionales y condiciones políticas presentes. En efecto,
es un acto radical insertar la palabra feminismo en el ciberespacio e
intentar interrumpir el flujo de los códigos machistas al declarar
abiertamente la intención de mestizar, hibridar, provocar y dislocar el
orden masculino de las cosas en el entorno de la Red.

Históricamente, el feminismo siempre ha implicado disrupciones
peligrosas, acciones abiertas y encubiertas y la guerra a las creencias,
tradiciones y estructuras patriarcales ofreciendo visiones utópicas de
un mundo sin roles de género. Un ciberfeminismo políticamente
inteligente y afirmativo, que use la sabiduría aprendida de luchas
pasadas, puede modelar una política descarada y disruptiva encaminada a
la deconstrucción de las condiciones patriarcales que actualmente
producen los códigos, lenguajes, imágenes y estructuras de la Red.

## La definición como estrategia política

Vincular términos como "ciber" y "feminismo" crea una nueva formación
crucial en la historia de los feminismos y los medios electrónicos. Cada
parte del término necesariamente modifica el significado de la otra. El
"feminismo" o, más propiamente, los "feminismos" han sido entendidos
como un movimiento transnacional histórico –-y contemporáneo–- por la
justicia y la libertad de las mujeres, que depende de la participación
activista en grupos vinculados locales, nacionales e
internacionales[^5]. Se enfoca en las condiciones materiales, políticas,
emocionales, sexuales y psíquicas que surgen de la construcción social
diferenciada de la mujer y los roles de género. Al vincular esto con
"ciber", que significa dirigir, gobernar, controlar, conjuramos la
posibilidad asombrosa del feminismo al timón de lo electrónico.  Los
ciberfeminismos podrían imaginar formas de vincular las prácticas
históricas y filosóficas del feminismo a los proyectos y redes
feministas contemporáneos, tanto dentro como fuera de la Red y a las
vidas y experiencias materiales de las mujeres en el circuito integrado,
tomando completamente en cuenta la edad, la etnia, la clase y las
diferencias económicas. Si los feminismos han de adecuarse a su
ciberpotencial, entonces debe mutar para ajustarse a las complejidades
de las realidades sociales y condiciones de vida conforme cambian por el
profundo impacto que las tecnologías de la comunicación y la
tecnociencia tienen en nuestras vidas.

[^5]: Usar el término "feminismo" es muy distinto de usar el término
  "mujer", aunque quizá deberíamos considerar usar el término
  "cibermujerismo" (cyberwomanism), que reconoce las críticas del
  feminismo racista blanco justamente hechas por Audre Lorde, Alice
  Walker y bell hooks, entre otras.

Mientras que rehusar la definición parece una táctica atractiva, no
jerárquica y anti-identitaria, de hecho cae en las manos de aquellos que
preferirían un quietismo cibernético: dale computadoras a unas cuantas
mujeres afortunadas para que jueguen y se van a callar y dejarán de
quejarse. Las ciberfeministas deben de ser extremadamente cautas y
críticas hacia esta actitud. El acceso a internet es aun un privilegio y
de ninguna manera se debe considerar un derecho universal (ni es
necesariamente útil o deseable para toda la gente). Mientras que la
brillante mercadotecnia ha convertido la posesión de un PC en algo tan
imperativo como un teléfono, las computadoras de hecho son poderosas
herramientas que pueden proporcionar a su poseedor una ventaja política
(la computadora personal es la computadora política). Si internet es
cada vez más el canal por el que mucha gente (en los países
superdesarrollados) recibe la mayor parte de su información, entonces
importa mucho cómo participan las mujeres en la programación,
establecimiento de política y formaciones de contenido de la Red, pues
la información que circula en ella necesita ser contextualizada tanto
por el receptor como por el emisor. En Internet, el feminismo tiene una
nueva audiencia transnacional que necesita ser educada en su historia y
sus condiciones contemporáneas según los diferentes países. Para muches,
el ciberfeminismo podría ser la entrada al discurso y el activismo
feministas. A pesar de que hay una gran cantidad de información sobre el
feminismo disponible en la Red –-y se están abriendo nuevos sitios todo
el tiempo–-, hay que recordar que mientras más se pueda contextualizar
políticamente esta información, y vincularla a las prácticas, el
activismo y las condiciones de la vida cotidiana, es más probable que
sirva para ayudar a conectar y movilizar a la gente[^6]. Un ejemplo
importante es el de la Red Zamir ("por la paz") de correo electrónico y
BBS, que fue creada después del inicio de la guerra civil en Yugoslavia
en 1991 para reunir a les activistas por la paz de Croacia, Serbia,
Eslovenia y Bosnia a través de las fronteras por medio de servidores en
Alemania. La cuestión es que las computadoras son más que herramientas
divertidas, juguetes de consumo o máquinas de placer personal... son las
herramientas del amo y tienen muy diversos usos y significados para las
diferentes poblaciones. Se necesitan capitanes hábiles para navegar
estos canales.

[^6]: Véanse, por ejemplo, los listados de mil sitios feministas o
  relacionados con las mujeres en Shana Penn [-@penn-1997].

Las ciberfeministas quieren evitar los nocivos errores de la exclusión,
la lesbofobia, la corrección política y el racismo, que a veces formaron
parte del pensamiento, el conocimiento y la experiencia feministas
pasados y los análisis y estrategias feministas acumulados hasta ahora
son cruciales para continuar con su trabajo. Si el objetivo es crear una
política feminista en la Red y dar poder a las mujeres, las
ciberfeministas deben reinterpretar y reubicar el análisis, la crítica,
las estrategias y la experiencia feministas para encontrar y contestar
nuevas condiciones, nuevas tecnologías y nuevas formaciones. La
(auto)definición puede ser una propiedad emergente que surge de la
práctica y cambia con los movimientos del deseo y la acción. La
definición puede ser fluida y afirmativa –-una declaración de
estrategias, acciones y metas. Puede crear una solidaridad crucial en la
casa de la diferencia –-solidaridad, más que unidad o consenso–-, una
solidaridad que es la base para una acción política efectiva.

La ciberfeministas tienen demasiado en juego como para alejarse de las
estrategias y acciones políticas duras por miedo a peleas internas,
ideologizaciones y diferencias políticas. Si yo prefiriera ser una
ciberfeminista que una diosa, quiero saber por qué y estar dispuesta a
decirlo.

## Una célula ciberfeminista

¿Cómo se podrían organizar las ciberfeministas para trabajar por un
ambiente feminista político y cultural en la Red? ¿Cuáles son las
diversas áreas de investigación feminista y actividad cibernética que
están comenzando a surgir como práctica ciberfeminista? La Primera
Internacional Ciberfeminista (IC) de Kassel sirve como ejemplo de la
organiz(m)ación feminista en la Red.

La responsabilidad de organizar los días de trabajo de la IC fue tomada
por OBN (_Old Boys Network_) –-un grupo ad hoc de alrededor de seis
mujeres–- en consulta en línea con todas las participantes. Debido a las
comunicaciones en línea entre las directivas de OBN y las participantes,
las relaciones colaborativas de trabajo y el contenido de las reuniones
ya estaba establecido para el momento en que las participantes se
reunieron cara a cara en Kassel. Un grupo cambiante y diverso de más de
treinta mujeres (autoseleccionadas por invitación abierta a las miembras
de la lista FACES, con un núcleo de alrededor de diez), participó en la
IC.

Desde el primer día, este proceso colaborativo –-una forma recombinante
de procesos de grupo feministas, autoorganización anárquica y liderazgo
rotativo–- siguió desarrollándose entre mujeres de más de ocho países y
de diferentes extracciones económicas, étnicas, profesionales y
políticas. Cada día comenzaba con una reunión de las participantes en el
Espacio de Trabajo Híbrido para trabajar en diversos grupos de trabajo
(texto, prensa, técnicas, fiesta de clausura, etc.) y para organizar el
programa público del día. A esto le seguían tres horas de conferencias
públicas y presentaciones para audiencias de documentación.

Después, el grupo cerrado se reunía de nuevo para comer y discutir
asuntos como la definición del ciberfeminismo, los objetivos del grupo y
las acciones y planes futuros. El trabajo se dividía según la
inclinación y la experiencia; no había una lista de deberes y no se
esperaba que todo el mundo trabajara el mismo número de horas. Los
horarios flexibles permitían la convivencia, las acciones impulsivas,
las lluvias de ideas y el tiempo privado. Se mantenía electrónicamente
una conexión constante con la lista FACES.

Prácticamente todas las actividades de grupo eran grabadas,
videograbadas y fotografiadas. Las computadoras personales de las
participantes se colocaron en el área abierta de trabajo/reunión y la
mayoría de conferencias se acompañaban de imágenes proyectadas de los
sitios web de las conferencistas. Una participante enseñó al grupo a
establecer conexiones _CUSeeMe_ y siguió participando virtualmente una
vez se tuvo que ir[^7] y dos jóvenes rusas que intentaban incorporarse a
la IC en Kassel, enviaban por fax un diario de su viaje ilegal mientras
saltaban de país en país intentando evadir problemas de visado. Así,
hubo un interesante juego entre la virtualidad y la presencia física.
Las interacciones cara a cara fueron mucho más intensas y energizantes
que las comunicaciones virtuales y forjaron grados distintos de afinidad
entre diversas individuas y subgrupos, mientras que al mismo tiempo
hicieron que las diferencias de todo tipo fueran más palpables. Las
lluvias de ideas y las acciones espontáneas parecían surgir más
rápidamente de las reuniones cara a cara. La oportunidad para las
sesiones inmediatas de pregunta-respuesta y la discusión extendida
después de las conferencias también permitieron intercambios más íntimos
y profundos que los usualmente posibles por medio de las comunicaciones
en línea. Pero lo más importante, todas las presentaciones,
entrenamientos y discusiones se llevaron a cabo en un contexto de
intenso debate sobre el feminismo, lo que produjo una conciencia
constante de la relación vivida entre las mujeres y la tecnología.

La amplia variedad de contenidos presentada en las conferencias,
proyectos de Internet y talleres tocó muchos de los temas más calientes
que les preocupan a las ciberfeministas: teorías de la visibilidad de la
diferencia sexual en la Red; auto-representaciones digitales de mujeres
en línea como avatares y cuerpos de datos; análisis de representaciones
de género, sitios de sexo, cibersexo y pornografía feminista;
estrategias de fusión de géneros e hibridismo para combatir los
estereotipos, el esencialismo y las representaciones sexistas de las
mujeres; el feminismo como "buscador"; los peligros del deseo fetichista
de información y la paranoia creada por las nuevas tecnologías; la
diseminación del conocimiento sobre las mujeres en la historia; estudios
sobre las diferencias entre les programadores y hackers varones y
mujeres; modelos feministas de educación tecnológica; asuntos de salud
de mujeres conectadas; y la discusión sobre cómo organizar y apoyar la
vinculación en red de proyectos feministas en diferentes países[^8].

[^7]: _CUSeeMe: See You, See Me_, es decir: "Te veo y me ves". Es un
  producto de bajo coste para videoconferencias por internet.

[^8]: Para más información sobre la Primera Internacional Ciberfeminista
  y las comunicaciones, véase <http://www.icf.de/obn>

Las ganancias principales de las discusiones de la IC fueron la
confianza, la amistad, un entendimiento más profundo y tolerancia a las
diferencias; la habilidad para mantener discusiones sobre asuntos
controversiales y divisorios sin la ruptura del grupo; y la mutua
educación sobre asuntos de mujeres inmersas en la tecnología, así como
una comprensión más clara del terreno para la intervención
ciberfeminista.

Aunque la IC no resultó en una lista formal de objetivos, acciones y
planes concretos, llegamos a un acuerdo general en áreas que necesitan
trabajo e investigación ulteriores. Una preocupación continua es cómo
lograr que el ciberfeminismo sea más visible y efectivo para llegar a
diversas poblaciones de mujeres que usan la tecnología. Las opciones
discutidas incluyeron crear un motor de búsqueda ciberfeminista que
pueda ligar sitios feministas estratégicos; informes país-por-país de
actividad en red y ciberorganización para mujeres; formar coaliciones
con mujeres tecnólogas, programadoras, científicas y hackers, con el fin
de vincular la teoría, el contenido y la práctica feministas en la Red
con la investigación e invención tecnológicas; proyectos educativos
(para varones y mujeres) en tecnología, programación y diseño de
software y hardware que apunten a las construcciones tradicionales de
género y a las desviaciones insertadas en la tecnología; y más
investigación sobre cómo la continua reestructuración global del trabajo
de las mujeres resulta de cambios constantes introducidos por la
tecnología de la información.

"El (ciber)feminismo es un buscador a través del cual se ve la
vida."[^9] Si las feministas desean investigar, teorizar, trabajar
prácticamente y hacer visible cómo las mujeres (y otres) en todo el
mundo son afectadas por las nuevas tecnologías de la comunicación, la
tecnociencia y el dominio capitalista de las redes de comunicación
global, deben comenzar por formular claramente los objetivos y las
posturas políticas ciberfeministas.

Las ciberfeministas tienen la oportunidad de crear nuevas formulaciones
de teoría y práctica feministas que apunten las nuevas y complejas
condiciones sociales, culturales y económicas creadas por las
tecnologías globales. Los usos estratégicos y políticamente inteligentes
pueden facilitar el trabajo de un movimiento transnacional que pretende
infiltrar y asaltar las redes de poder y comunicación por medio de
proyectos activistas-feministas de solidaridad, educación, libertad,
visión y resistencia. Para ser efectivas en la creación de un entorno
feminista politizado en la Red, que desafíe sus estructuras actuales de
género, raza, edad y clase, las ciberfeministas necesitan aprender de
las investigaciones y estrategias de la historia feminista de vanguardia
y su crítica del patriarcado institucionalizado. Al afirmar nuevas
posibilidades para las mujeres en el ciberespacio, las ciberfeministas
deben criticar las construcciones utópicas y míticas en la Red y buscar
trabajar con otros grupos cibernéticos de resistencia en coaliciones
activistas. Las ciberfeministas necesitan declararse solidarias con las
iniciativas transnacionales feministas y poscoloniales y trabajar para
usar su acceso a las tecnologías de la comunicación y las redes
electrónicas, para apoyar dichas iniciativas.

[^9]: Alla Mitrofanova, presentación en la Primera Internacional
  Feminista en Kassel, septiembre de 1997.

# Bibliografía

